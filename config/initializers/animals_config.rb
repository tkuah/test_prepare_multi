# Create a new connection class, then connect to it
class TempAnimalsRecord < ActiveRecord::Base
  self.abstract_class = true

  connects_to database: { writing: :animals }

  def self.current_database
    TempAnimalsRecord.connection.select_one('SELECT current_database()')
  end
end

# Open a connection to the animals database
puts TempAnimalsRecord.current_database
