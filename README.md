# README

Test application to test db:test:prepare behaviour with multiple databases

To trigger the bug, run:

```
RAILS_ENV=test be rails db:test:prepare
```
